##Simple Chat 

_This program was written as a part of ECE-156 (Computer Network Architecture) course assignment at Duke University._ 

The application demonstrate the basic operation of peer-to-peer communication. The application consists of a client and a server utilizing TCP socket connections. The client is able to connect to the server, obtain a list of available clients and then connect and relay text messages directly to another clients without further help from the server.