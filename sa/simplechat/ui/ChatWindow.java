package sa.simplechat.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
 * UI for Chat/Server Window
 * 
 * @author Sandip Agrawal
 */
@SuppressWarnings("serial")
public class ChatWindow extends JFrame {
    protected JTextArea textArea;
    protected JTextField textField;

    public ChatWindow(String title) {
        setMinimumSize(new Dimension(480, 480));
        setTitle(title);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        JPanel mypanel = new JPanel(new BorderLayout());

        textArea = new JTextArea(25, 40);
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        mypanel.add(textArea, BorderLayout.NORTH);

        JScrollPane scrolltext = new JScrollPane(textArea);
        scrolltext.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        mypanel.add(scrolltext);

        textField = new JTextField(41);
        textField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                onTextEntered(evt);
            }
        });
        mypanel.add(textField, BorderLayout.SOUTH);

        mypanel.setBorder(BorderFactory.createTitledBorder("Message"));
        getContentPane().add(mypanel);

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                onWindowClosing(we);
            }
        });


        validate();
        setVisible(true);
    }

    /** the listener method for when the window is closed by pressing the "X" button
     * 
     * @param we
     */
    protected void onWindowClosing(WindowEvent we) {
    }

    /**
     * Called during the ActionListner event for TextArea
     * 
     * @param evt
     */
    public void onTextEntered(ActionEvent evt) {
    }

    public void appendText(String msg){
        textArea.append(msg);
    }
    
}
