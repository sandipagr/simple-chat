package sa.simplechat.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.Set;

/**
 * A server thread that interacts with one client.
 * 
 * @author Abhishek Thapa
 * @author Sandip Agrawal
 */
public class ServerThread extends Thread {

    private Socket clientSocket; // socket that is connected to the client
    private String clientName; // name of the client

    // reference to the list of clients, this object is shared among all
    // instances of ServerThread class and ServerConnection
    private Map<String, String[]> myClients;

    private BufferedReader inputStream;
    private PrintWriter outputStream;

    /**
     * Create a new server thread for a client with:
     * 
     * @param s
     *            the socket connected to the client
     * @param m
     *            the list of clients connected.
     * @throws IOException
     *             if I/O streams couldn't be initialized, caught by calling
     *             thread
     */
    public ServerThread(Socket s, Map<String, String[]> m) throws IOException {
        clientSocket = s;
        myClients = m;
        inputStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        outputStream = new PrintWriter(clientSocket.getOutputStream(), true);
    }

    public void run() {

        // get the client name and port that client is listening on
        try {
            getClientName();
        } catch (IOException e) {
            System.err.println("Input read of client name failed");
        }

        if (clientName != null)
            System.out.println(clientName + " " + clientSocket.getInetAddress());

        // send the list of connected clients
        sendList();

        try {
            while (true) {
                String fromClient = inputStream.readLine();

                // change user nick name command "\\name"
                if (fromClient.startsWith("\\name")) {
                    String[] temp = fromClient.split("\\s");
                    if (temp.length == 2 && temp[0].equals("\\name")) {
                        synchronized (this) {
                            if (myClients.containsKey(temp[1])) {
                                outputStream.println("name conflict");
                            } else {
                                myClients.put(temp[1], myClients.get(clientName));
                                myClients.remove(clientName);

                                clientName = temp[1];
                                outputStream.println("name ok");
                            }
                        }
                    }
                }
                // user wants list again
                else if (fromClient.equals("\\sendList")) {
                    sendList();
                }
                // user wants to quit. close I/O streams and socket.
                // remove this client from client list and exit
                else if (fromClient.equals("\\quit")) {
                    quit();
                    break;
                }
            }
        } catch (IOException e) {
            System.err.println("Operation failed");
        }

    }

    /**
     * Get the client name, quit if the client closed the window Assumes that
     * the input is either quit, or clientName + port number
     * 
     * @throws IOException
     */
    private void getClientName() throws IOException {
        String clientInput;
        String[] fields;
        while (true) {
            Set<String> clients;
            synchronized (this) {
                clients = myClients.keySet();
            }

            clientInput = inputStream.readLine();

            // if the user closed the window at this point
            if (clientInput.equals("\\quit")) {
                quit();
                return;
            }

            // otherwise interpret the input as clientname and port number
            fields = clientInput.split("\\s");

            if (!clients.contains(fields[0])) {
                outputStream.println("name ok");
                synchronized (this) {
                    String[] address = { clientSocket.getInetAddress().toString().substring(1), fields[1] };
                    clientName = fields[0];
                    myClients.put(clientName, address);
                }
                break;
            }
            outputStream.println("name conflict");
        }
    }

    /**
     * Sends the list of connected clients to this client.
     */
    private void sendList() {
        synchronized (this) {
            if (myClients.keySet().size() > 0) {
                for (String s : myClients.keySet()) {
                    if (!clientName.equals(s)) {
                        String toClient = s + " " + myClients.get(s)[0] + " " + myClients.get(s)[1];
                        outputStream.println(toClient);
                    }
                }
            }
        }
        outputStream.println("Done");
    }

    /**
     * Close I/O streams and socket, remove from client list
     */
    private void quit() {
        System.out.println(clientName + " quitting");
        try {
            inputStream.close();
            outputStream.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        synchronized (this) {
            myClients.remove(clientName);
        }
    }
}
