package sa.simplechat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Main Server class that listens to incoming client connections. Upon each
 * connections, it spawns a new thread and runs ServerThread
 * 
 * @author Abhishek Thapa
 * @author Sandip Agrawal
 * 
 */
public class ServerConnection {

    private ServerSocket mySocket; // server socket that listens to new clients

    public Map<String, String[]> myClients; // map of client name to ip and port

    private int PORT = 21346; // port on which server listens

    public ServerConnection() {
        myClients = new HashMap<String, String[]>();
        try {
            mySocket = new ServerSocket(PORT);
        } catch (IOException e) {
            System.err.println("Couldn't instantiate server socket");
        }
    }

    public void run() {
        // dummy socket variable that would be assigned to new client connections
        Socket clientSocket = null;
        try {
            while (true) {
                // listen to new connection
                clientSocket = mySocket.accept();
                synchronized (myClients) {
                    System.out.println("connection " + myClients.size()); // starts at connection 0

                    ServerThread s = new ServerThread(clientSocket, myClients); // opens a new thread for this client
                    s.start();
                }
            }
        } catch (IOException e) {
            System.err.println("Accept failed.");
        }
    }

    public static void main(String[] args) {
        ServerConnection s = new ServerConnection();
        s.run();
    }
}
