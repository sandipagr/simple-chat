package sa.simplechat.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

/** 
 * Main class for the client side chat
 * @author Sandip Agrawal
 *
 */
public class ClientChatApplication {
 
    /**
     * Continuously listens for a connections from other user
     */
    public void connection(ServerSocket server, ServerChatWindow serverChat) {
        Socket acceptingSocket = null;
        boolean accepting = true;
        while (accepting) {
            try {
                acceptingSocket = server.accept(); 
                new Thread(new ClientChatWindow(serverChat.getUserName(), acceptingSocket)).start();
            } catch (IOException e) {
                break;
            }
        }
    }

    /**
     * Main method for the client side application 
     * 
     */
    public static void main(String args[]) throws Exception {
        if(args.length==0){ 
            int port = (new Random()).nextInt(60000) + 2000;
            args = new String[] {port + ""}; 
        }

        Socket clientsocket = new Socket();
        clientsocket.connect(new InetSocketAddress("192.168.1.4", 21346), 10000);
        
        // make a listening socket for other client
        ServerSocket tempServer = new ServerSocket(Integer.parseInt(args[0]));
        
        //Open a GUI to talk with server
        ServerChatWindow serverChatWindow = new ServerChatWindow("Simple Chat", Integer.parseInt(args[0]), clientsocket, tempServer);
        serverChatWindow.appendText("Connected to Server with Listening Port " + args[0] + "\n\n");
        serverChatWindow.appendText("Please Input your Login Name: \n");

        ClientChatApplication tcp = new ClientChatApplication();
        tcp.connection(tempServer, serverChatWindow);
    }
}