package sa.simplechat.client;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import sa.simplechat.ui.ChatWindow;

/**
 *  Handles communication between client and server
 *  
 * @author Sandip Agrawal
 *
 */
@SuppressWarnings("serial")
public class ServerChatWindow extends ChatWindow {
    
    private Socket socket;
    private ServerSocket ssocket;
    private DataOutputStream outStream;
    private BufferedReader inFromServer;
    
    public Map<String, String[]> clients;
    boolean firstEntry = true;
    private int listeningPort;
    private String userName;
    
    public ServerChatWindow(String title, int listeningPort, Socket s, ServerSocket ssckt) {
        super(title);

        ssocket = ssckt;
        socket = s;
        
        try {
            outStream = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e1) {
            System.out.println("Unable to instantiate output stream");
        }
        
        try {
            inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e1) {
            System.out.println("Unable to instantiate Input stream");
        }

        clients = new HashMap<String, String[]>(); 
        this.listeningPort = listeningPort;
    }
       
    @Override
    protected void onWindowClosing(WindowEvent we) {
        quit();
    }

    @Override
    public void onTextEntered(ActionEvent evt) {
        String userText = textField.getText();
        textField.setText("");
        
        //First User Entry has to be the login name   
        if (firstEntry) {
            try {
                userName = userText;
                outStream.writeBytes(userText + " " + listeningPort + "\n");
                
                String response = inFromServer.readLine();
                if (response.equals("name ok")) {
                    textArea.append("Welcome, " + userName + "!\n");
                    firstEntry = false;
                    getUserList();
                }

                if (response.equals("name conflict")) {
                    textArea.append("ERROR: Name Already Exists...Try Different Name" + "\n");
                    firstEntry = true;
                }
            } catch (IOException e) {
                System.out.println("Failed to send Contact information.");
            }
        } else {
            // User wants to change the name
            if (userText.startsWith("\\name")) {
                changeName(userText);
            } else if (userText.equals("\\quit")) {
                quit();
            } else if (userText.startsWith("\\connect")) {
                connectToClient(userText);
            } else {
                try {
                    outStream.writeBytes(userText + "\n");
                } catch (IOException e) {
                    System.out.println("Failed to send: " + userText);
                }
                if (userText.equals("\\sendList")) {
                    getUserList();
                }            
            }
        }
        
    }

    private void getUserList() {
        try {
            String input = inFromServer.readLine();
            clients.clear();
            while (!input.equals("Done")) {
                textArea.append(input + "\n");
                String[] temp = input.split("\\s");
                String[] temp1 = {temp[1], temp[2]};
                clients.put(temp[0], temp1);
                input = inFromServer.readLine();
            }
            
            if(clients.size() < 1){
                textArea.append("No other user is available to chat.\n");
            }
            
        } catch (IOException e) {
            System.out.println("Cannot obtain the Client list");
        }
    }

    private void connectToClient(String userText) {
        String username = userText.substring(9);
        textArea.append("Connecting to " + username + "...\n");
        String[] userIpPort = clients.get(username);
        if(userIpPort == null){
            textArea.append("Could not find the username " + username + " in the user list.\n");
            return;
        }
        
        Socket f = new Socket();
        try {
            f.connect(new InetSocketAddress(userIpPort[0], Integer.parseInt(userIpPort[1])), 10000);
            new Thread(new ClientChatWindow(userName, f)).start();
        } catch (NumberFormatException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            System.out.println("Unable to connect to the client");
            throw new RuntimeException(e);
        }        
    }

    private void changeName(String userText) {
        try {
            outStream.writeBytes(userText + "\n"); 
            String response = inFromServer.readLine();
            if (response.equals("name ok")) {
                userName = userText.substring(6);
                textArea.append(" Name Succesfully Modified" + "\n");
            }
            if (response.equals("name conflict")) {
                textArea.append("ERROR: Name Already Exists...Try Different Name" + "\n");
            }
        } catch (IOException e) {
            System.out.println("Could not send name change request");
        }        
    }
 
    private void quit(){
        try {
            outStream.writeBytes("\\quit\n");
            inFromServer.close();
            outStream.close();
            ssocket.close();
            socket.close();
            dispose();
        } catch (IOException e) {
            System.out.println(e);
        }        
    }
 
    public String getUserName(){
        return userName;
    }
}
