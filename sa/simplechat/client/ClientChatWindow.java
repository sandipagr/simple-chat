package sa.simplechat.client;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import sa.simplechat.ui.ChatWindow;

/**
 * Handles Communication between client-client
 * @author Sandip Agrawal
 */
@SuppressWarnings("serial")
public class ClientChatWindow extends ChatWindow implements Runnable {

    private Socket socketforclient;
    private DataOutputStream clientoutStream;
    private BufferedReader inFromClient;
    private String userName;

    public void run() {
        try {
            clientoutStream = new DataOutputStream(socketforclient.getOutputStream());
            inFromClient = new BufferedReader(new InputStreamReader(socketforclient.getInputStream()));
            while (!socketforclient.isClosed()) { //socket is connected
                try {
                    String clientInput = inFromClient.readLine();
                    if(clientInput != null){
                        if(clientInput.equals("\\quit")){
                            textArea.append("The user has left the conversation.");
                            textField.setEnabled(false);
                            quit();
                        } else {
                            textArea.append(clientInput + "\n");
                        }
                    }
                } catch (IOException e) {
                    textArea.append("CONNECTION TO CLIENT LOST!!");
                }
            }
        } catch (IOException e) {
            textArea.append(" NO INPUT/OUTPUT DATA STREAM ERROR!!");
        }
    }

    public ClientChatWindow(String userName, Socket s) {
        super(userName);
        this.userName = userName;
        setResizable(false);
        
        socketforclient = s;
    }

    @Override
    protected void onWindowClosing(WindowEvent we) {
        quit();
    }

    @Override
    public void onTextEntered(ActionEvent evt) {
        String userText = textField.getText();
        textField.setText("");

        textArea.append(userName + ": " + userText + "\n");
        String msgToSend;

        if (userText.equals("\\quit")) {
            msgToSend = userText + "\n";            
        } else {
            msgToSend = userName + ": " + userText + "\n";
        }
        
        try {
            clientoutStream.writeBytes(msgToSend);
        } catch (IOException e) {
            textArea.append("Failed to send: " + userText + "\n");
        }
        
        if (userText.equals("\\quit")) {
            quit();
            dispose();
        }
    }
    
    private void quit(){
        try {
            inFromClient.close();
            clientoutStream.close();
            socketforclient.close();
        } catch (IOException e) {
            System.out.println("Failed to quit");
        }
    }
}
